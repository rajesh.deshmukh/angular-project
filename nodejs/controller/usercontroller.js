const express = require('express');
var router = express.Router();

var { User } = require('../model/user');

var objectId = require('mongoose').Types.objectId;

//=> localhost:4000/users/


router.get('/', (req, res) => {
    User.find((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error in retriving users');
        }
    });
});

router.get('/:id', (req, res) => {
    User.findById(req.params.id, (err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Did not get selected UserId');
        }
    });
});

router.post('/', (req, res) => {
    var userobj = new User({
        name: req.body.name,
        office: req.body.office,
        salary: req.body.salary
    });
    userobj.save((err, docs) => {
        if (!err) {
            res.send(docs);
            console.log('User data save');
        } else {
            console.log('User data is not submitted');
        }
    });
});

router.put('/:id', (req, res) => {
    // if (!objectId.isValid(req.params.id))
    //     return res.status(400).send(`Record not found : ${req.params.id}`);

    var userobj = new User({
        _id:req.params.id,
        name: req.body.name,
        office: req.body.office,
        salary: req.body.salary
    });

    // User.findByIdAndUpdate(req.params.id, { $set: userobj }, { new: true }, (err, doc) => {
    //     if (!err) {
    //         res.send(docs);
    //     } else {
    //         console.log('User id is incorrect');
    //     }
    // });

    User.updateOne({_id: req.params.id}, userobj).then(
        () => {
            res.status(201).json({
            message: 'User udated successfully!'
            });
        }
    ).catch(
        (err) => {
            res.status(400).json({
                err:err
            });
        }
    );
});

router.delete('/:id', (req, res) => {
    console.log("in delete re");
    // if(!ObjectId.isValid(req.params.id))
    //      return res.status(400).send(`No record with given id : ${req.params.id}`);

    User.findByIdAndRemove(req.params.id, (err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in retriving Employees :' + JSON.stringify(err, undefined, 2)); }
    });
});



module.exports = router;