const express = require('express');
const cors = require('cors');

const bodyParser = require('body-parser');
var userController = require('./controller/usercontroller.js');

const { mongoose } = require('./db.js');
var app = express();
app.use(bodyParser.json());
app.use(cors({origin: 'http://localhost:4200'}));


app.listen(4000, ()=> {
    console.log('server started on port 4000..');
});
app.use('/users',userController);